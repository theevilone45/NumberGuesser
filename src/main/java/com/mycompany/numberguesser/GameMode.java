package com.mycompany.numberguesser;

public enum GameMode {
    PLAYER_GUESSING,
    CPU_GUESSING,
    VERSUS,
    MULTIPLAYER
}
