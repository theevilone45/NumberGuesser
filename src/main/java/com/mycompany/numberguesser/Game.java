package com.mycompany.numberguesser;

import java.text.MessageFormat;

enum GameState {
    RUNNING,
    QUIT
}

public class Game {
    private final UserInterface mUserInterface;
    private GameState mGameState;
    private final Player mPlayer;
    private final ModeLogics mModeLogics;

    Game() {
        mUserInterface = new UserInterface();
        mPlayer = new Player(mUserInterface.readNick());
        mGameState = GameState.RUNNING;
        mModeLogics = new ModeLogics(mUserInterface);
    }

    public void mainLoop() {
        if (!SaveDir.createIfNotExists()) {
            System.out.println("Critical error occurred. Exiting.");
            mGameState = GameState.QUIT;
        }
        while (mGameState != GameState.QUIT) {
            showCurrentPlayer();
            dispatchMenu();
        }
    }

    public void dispatchMenu() {
        mUserInterface.showMenu();
        MainMenuOption opt = mUserInterface.readMainMenu();
        switch (opt) {
            case PLAYER_GUESSING_MODE -> runGameMode(GameMode.PLAYER_GUESSING, false);
            case CPU_GUESSING_MODE -> runGameMode(GameMode.CPU_GUESSING, true);
            case VERSUS_MODE -> runGameMode(GameMode.VERSUS, false);
            case MULTIPLAYER -> runGameMode(GameMode.MULTIPLAYER, false);
            case SHOW_SCORES -> showScores();
            case CHANGE_NAME -> changePlayer();
            case SET_DIFFICULTY -> setDifficulty();
            case QUIT -> quitGame();
            default -> throw new IllegalStateException("Unexpected value: " + opt);
        }
    }

    private void setDifficulty()
    {
        mUserInterface.showDifficultyMenu();
        mModeLogics.setDifficulty(mUserInterface.readDifficulty());
    }

    private void showCurrentPlayer() {
        mUserInterface.showDelimiter();
        String pattern = "Nick: {0} | Difficulty: {1}";
        String formatted = MessageFormat.format(pattern, mPlayer.getNick(), mModeLogics.getDifficulty().toString());
        System.out.println(formatted);
    }

    private void runGameMode(GameMode mode, boolean higherIsBetter)
    {
        DifficultyLevel level = mModeLogics.getDifficulty();
        int result = 0;
        switch(mode)
        {
            case PLAYER_GUESSING -> {
                result = mModeLogics.runPlayerGuessingMode();
            }
            case CPU_GUESSING -> {
                result  = mModeLogics.runCpuGuessingMode();
            }
            case VERSUS -> {
                result = mModeLogics.runVersusMode();
            }
        }
        int oldResult = mPlayer.getScore(mode, level);
        // higher is better
        boolean isNewHighScore = (higherIsBetter) ? (result > oldResult) : (result < oldResult);
        if (isNewHighScore || oldResult == 0) {
            System.out.println("New high score: " + result);
            mPlayer.setScore(mode, level, result);
        }
    }

    private void showScores() {
        mUserInterface.showDelimiter();
        mPlayer.showScores();
    }

    private void quitGame()
    {
        mPlayer.saveToFile();
        mGameState = GameState.QUIT;
    }

    private void changePlayer() {
        mPlayer.saveToFile();
        mPlayer.setNick(mUserInterface.readNick());
        mPlayer.readFromFile();
    }

}
