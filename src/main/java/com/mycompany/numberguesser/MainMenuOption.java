package com.mycompany.numberguesser;

public enum MainMenuOption {
    PLAYER_GUESSING_MODE,
    CPU_GUESSING_MODE,
    VERSUS_MODE,
    MULTIPLAYER,
    SET_DIFFICULTY,
    SHOW_SCORES,
    CHANGE_NAME,
    QUIT
}
