package com.mycompany.numberguesser;

/**
 * This file contains definitions of gameplay
 * game supports four different game modes
 * - CPU chooses number and Player guesses
 * - Player chooses number and CPU guesses
 * - CPU and player guesses in turns
 * - Multiplayer
 */

import java.text.MessageFormat;
import java.util.Random;

public class ModeLogics {
    private DifficultyLevel mLevel;
    private final Random mGenerator;
    private final UserInterface mUserInterface;

    ModeLogics(UserInterface ui) {
        mGenerator = new Random();
        mUserInterface = ui;
        mLevel = DifficultyLevel.EASY;
    }

    public void setDifficulty(DifficultyLevel value)
    {
        mLevel = value;
        if(mLevel == DifficultyLevel.CUSTOM)
        {
            int min = 0;
            int max = 0;
            while(true) {
                min = mUserInterface.readInt("Enter lower bound: ");
                max = mUserInterface.readInt("Enter upper bound: ");
                if(min >= max)
                {
                    System.out.println("Lower bound should be well... lower than upper bound ;)");
                    continue;
                }
                break;
            }
            mLevel.setCustomRange(min, max);
        }
    }

    public DifficultyLevel getDifficulty()
    {
        return mLevel;
    }

    private boolean checkNumber(int userNumber, int numberToGuess)
    {
        if (userNumber > numberToGuess) {
            System.out.println("Your number was too high.");
        } else if (userNumber < numberToGuess) {
            System.out.println("Your number was too low.");
        } else {
            return true;
        }
        return false;
    }

    /**
     * CPU generate random number between 0 and MAX_NUMBER (based on difficulty setting)
     *
     * @return amount of tries
     */
    public int runPlayerGuessingMode() {
        int result = 0;

        int numberToGuess = generateRandomNumber();

        System.out.println("CPU chose number. Try to guess it.");
        String pattern = "Enter your lucky number [{0}, {1}]: ";
        String formatted = MessageFormat.format(pattern, mLevel.getRange().first, mLevel.getRange().second);
        while (true) {
            result++;
            int userNumber = mUserInterface.readInt(formatted);
            if(checkNumber(userNumber, numberToGuess)){
                String resultPattern = "You won! Your result: {0}";
                String resultFormatted = MessageFormat.format(resultPattern, result);
                System.out.println(resultFormatted);
                return result;
            }
        }
    }

    public int runCpuGuessingMode() {
        int chosenNumber = chooseNumber();

        int tries = 0;
        int l = mLevel.getRange().first, r = mLevel.getRange().second;
        while (l <= r) {
            int cpuGuess = (l + r) / 2;
            tries++;
            if (cpuGuess == chosenNumber) {
                System.out.println("CPU won in: " + tries + " tries");
                return tries;
            }
            System.out.println("CPU missed (" + cpuGuess + ") Trying again.");
            if (cpuGuess > chosenNumber) {
                r = cpuGuess - 1;
            }
            if (cpuGuess < chosenNumber) {
                l = cpuGuess + 1;
            }
        }
        return -1;
    }

    public int runVersusMode()
    {
        int numberToGuessByPlayer = generateRandomNumber();
        int numberToGuessByCpu = chooseNumber();
        String pattern = "Enter your lucky number [{0}, {1}]: ";
        String formatted = MessageFormat.format(pattern, mLevel.getRange().first, mLevel.getRange().second);

        boolean isPlayerStarting = coinFlip();
        System.out.println("CPU chose number. Try to guess it.");

        int userNumber;
        int tries = 0;
        if(isPlayerStarting)
        {
            System.out.println("You starting first. Good luck.");
            tries++;
            userNumber = chooseNumber();
            if(checkNumber(userNumber, numberToGuessByPlayer))
            {
                System.out.println("You won! Your result: 1");
                return 1;
            }
        }
        else
        {
            System.out.println("CPU starts first. Good luck");
        }
        int l = mLevel.getRange().first, r = mLevel.getRange().second;
        while (l <= r)
        {
            int cpuGuess = (l + r) / 2;
            tries++;
            if (cpuGuess == numberToGuessByCpu) {
                System.out.println("CPU won in: " + tries + " tries");
                return -1;
            }

            System.out.println("CPU missed. Your turn.");
            if (cpuGuess > numberToGuessByCpu) {
                r = cpuGuess - 1;
            }
            if (cpuGuess < numberToGuessByCpu) {
                l = cpuGuess + 1;
            }

            userNumber = mUserInterface.readInt(formatted);
            if(checkNumber(userNumber, numberToGuessByPlayer)){
                System.out.println("You won! Your result: " + tries);
                return tries;
            }
        }

        return -1;
    }

    private int generateRandomNumber() {
        int amountOfNumbers = mLevel.getRange().second - mLevel.getRange().first;
        return mGenerator.nextInt(amountOfNumbers + 1) + mLevel.getRange().first;
    }

    private int chooseNumber() {
        int result;
        String pattern = "Enter number for CPU to guess [{0}, {1}]: ";
        String formatted = MessageFormat.format(pattern, mLevel.getRange().first, mLevel.getRange().second);
        while (true) {
            System.out.print(formatted);
            result = mUserInterface.readInt();
            if (result >= mLevel.getRange().first && result <= mLevel.getRange().second) {
                break;
            }
            System.out.println("You chose number outside range. Please don't try to cheat ;)");
        }
        return result;
    }

    private boolean coinFlip()
    {
        return mGenerator.nextInt(2) == 1;
    }
}
