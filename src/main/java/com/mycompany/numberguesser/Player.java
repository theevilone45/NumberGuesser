package com.mycompany.numberguesser;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Scanner;

class ScoreRecord implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    public GameMode gameMode;
    public DifficultyLevel difficulty;
    public int score;

    ScoreRecord(GameMode gameMode, DifficultyLevel difficulty) {
        this.gameMode = gameMode;
        this.difficulty = difficulty;
        score = 0;
    }
}

public class Player {
    private static final String cExtension = ".dat";
    private String mNick;
    private ArrayList<ScoreRecord> mScores = new ArrayList<>();

    Player(String nick) {
        mNick = nick;
        initializeScore(GameMode.PLAYER_GUESSING);
        initializeScore(GameMode.CPU_GUESSING);
        initializeScore(GameMode.VERSUS);
        initializeScore(GameMode.MULTIPLAYER);

        readFromFile();
    }

    public String getNick() {
        return mNick;
    }

    public void setNick(String value) {
        mNick = value;
    }

    public Integer getScore(GameMode mode, DifficultyLevel level) {
        for (ScoreRecord record : mScores) {
            if (record.difficulty == level && record.gameMode == mode) {
                return record.score;
            }
        }
        return null;
    }

    public void setScore(GameMode mode, DifficultyLevel level, int value) {

        for (ScoreRecord record : mScores) {
            if (record.difficulty == level && record.gameMode == mode) {
                record.score = value;
                return;
            }
        }
    }

    public void saveToFile() {
        Path saveFilePath = Paths.get(SaveDir.getPath().toString(), getNick() + cExtension);
        try (ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(String.valueOf(saveFilePath)))) {
            writer.writeObject(mScores);
        } catch (IOException e) {
            System.out.println("Error occurred while saving file: " + e);
        }
    }

    public void readFromFile() {
        Path saveFilePath = Paths.get(SaveDir.getPath().toString(), getNick() + cExtension);
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(String.valueOf(saveFilePath)))) {
            Object obj = ois.readObject();
            if (obj instanceof ArrayList<?> arrayList) {
                if (!arrayList.isEmpty() && arrayList.getFirst() instanceof ScoreRecord) {
                    mScores = (ArrayList<ScoreRecord>) arrayList;
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Welcome new player: " + getNick());
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Error occurred while reading: " + e);
        }
    }

    public void showScores() {
        String pattern = "{0} - {1} - {2}";
        for (ScoreRecord record : mScores) {
            String score = (record.score > 0) ? String.valueOf(record.score) : "N/A";
            String formatted = MessageFormat.format(pattern, record.gameMode, record.difficulty, score);
            System.out.println(formatted);
        }
    }

    private void initializeScore(GameMode mode) {
        mScores.add(new ScoreRecord(mode, DifficultyLevel.EASY));
        mScores.add(new ScoreRecord(mode, DifficultyLevel.MEDIUM));
        mScores.add(new ScoreRecord(mode, DifficultyLevel.HARD));
        mScores.add(new ScoreRecord(mode, DifficultyLevel.INSANE));
        mScores.add(new ScoreRecord(mode, DifficultyLevel.CUSTOM));
    }
}
