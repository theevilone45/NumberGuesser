package com.mycompany.numberguesser;

public enum DifficultyLevel
{
    EASY,
    MEDIUM,
    HARD,
    INSANE,
    CUSTOM;

    private Tuple<Integer, Integer> mCustomRange = new Tuple<>(0,100);

    public void setCustomRange(int min, int max)
    {
        mCustomRange = new Tuple<>(min, max);
    }
    
    public Tuple<Integer, Integer> getRange()
    {
        switch(this)
        {
            case EASY -> {
                return new Tuple<>(0, 100);
            }
            case MEDIUM -> {
                return new Tuple<>(0, 10000);
            }
            case HARD -> {
                return new Tuple<>(0, 1000000);
            }
            case INSANE -> {
                return new Tuple<>(0, Integer.MAX_VALUE);
            }
            case CUSTOM -> {
                return mCustomRange;
            }
            default -> throw new IllegalStateException("Unexpected value: " + this);
        }
    }
}
