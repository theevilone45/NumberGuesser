package com.mycompany.numberguesser;

import java.text.MessageFormat;
import java.util.Scanner;

public class UserInterface {
    private final Scanner mScanner = new Scanner(System.in);

    public void showDelimiter() {
        System.out.println("--------------------------------");
    }

    public void showMenu() {
        showDelimiter();

        int i = 0;
        for (MainMenuOption opt : MainMenuOption.values()) {
            String pattern = "[{0}] {1}";
            String formatted = MessageFormat.format(pattern, i++, opt);
            System.out.println(formatted);
        }
    }

    public void showDifficultyMenu() {
        showDelimiter();
        int i = 0;
        for (DifficultyLevel opt : DifficultyLevel.values()) {
            String pattern = "[{0}] {1}";
            String formatted = MessageFormat.format(pattern, i++, opt);
            System.out.println(formatted);
        }
    }

    public MainMenuOption readMainMenu() {
        int lastPossibleInput = MainMenuOption.values().length - 1;
        String pattern = "Enter menu option [0-{0}]: ";
        String formatted = MessageFormat.format(pattern, lastPossibleInput);
        System.out.print(formatted);

        int resultInt = readInt();
        while (true) {
            if (resultInt < 0 || resultInt > lastPossibleInput) {
                System.out.println("Please enter correct menu option.");
                continue;
            }
            break;
        }

        return MainMenuOption.values()[resultInt];
    }

    public DifficultyLevel readDifficulty() {
        int lastPossibleInput = DifficultyLevel.values().length - 1;
        String pattern = "Enter difficulty [0-{0}]: ";
        String formatted = MessageFormat.format(pattern, lastPossibleInput);
        System.out.print(formatted);

        int resultInt = readInt();
        while (true) {
            if (resultInt < 0 || resultInt > lastPossibleInput) {
                System.out.println("Please enter correct menu option.");
                continue;
            }

            break;
        }

        return DifficultyLevel.values()[resultInt];
    }

    public String readNick() {
        return readString("Enter your name: ");
    }

    public String readString(String message) {
        if (message != null) {
            System.out.print(message);
        }
        return mScanner.nextLine();
    }

    public String readString() {
        return readString(null);
    }

    public int readInt(String message) {
        if (message != null) {
            System.out.println(message);
        }
        int result;
        while (true) {
            try {
                result = Integer.parseInt(mScanner.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Please enter correct number.");
                continue;
            }
            break;
        }
        return result;
    }

    public int readInt() {
        return readInt(null);
    }
}
