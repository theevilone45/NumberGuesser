package com.mycompany.numberguesser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SaveDir {

    public static Path getPath() {
        String homeDir = System.getProperty("user.home");
        String dirName = "GuessNumberSaves";
        return Paths.get(homeDir, dirName);
    }

    public static boolean createIfNotExists() {
        Path path = getPath();
        try {
            if (!Files.exists(path)) {
                Files.createDirectory(path);
                System.out.println("Save directory created.");
            } else {
                System.out.println("Save directory already exists.");
            }
        } catch (IOException e) {
            System.out.println("Error while creating save directory.");
            return false;
        }
        return true;
    }
}
